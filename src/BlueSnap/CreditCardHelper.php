<?php

namespace Drupal\commerce_bluesnap\BlueSnap;

use Bluesnap\Models\PaymentSources;

/**
 * Helper methods related to BlueSnap credit cards.
 */
class CreditCardHelper {

  /**
   * Returns the map of supported credit cards.
   *
   * @return array
   *   A associative array of all supported credit card types, keyed by their
   *   BlueSnap IDs with their Commerce IDs as their values.
   */
  public static function creditCardTypeMap() {
    return [
      'AMEX' => 'amex',
      'DINERS' => 'dinersclub',
      'DISCOVER' => 'discover',
      'JCB' => 'jcb',
      'MASTERCARD' => 'mastercard',
      'VISA' => 'visa',
    ];
  }

  /**
   * Finds the card of the given type and 4 digits in the given payment sources.
   *
   * @param \Bluesnap\Models\PaymentSources $payment_sources
   *   The payment sources object.
   * @param string $bluesnap_type
   *   The BlueSnap credit card type.
   * @param string $last_four_digits
   *   The last 4 digits of the card.
   *
   * @return \Bluesnap\Models\CreditCard|null
   *   The credit card object, or NULL if the card was not found in the given
   *   payment sources.
   */
  public static function findCreditCardInPaymentSources(
    PaymentSources $payment_sources,
    string $bluesnap_type,
    string $last_four_digits
  ) {
    foreach ($payment_sources->creditCardInfo as $card_info) {
      $card = $card_info->creditCard;
      if ($card->cardType !== $bluesnap_type) {
        continue;
      }
      if ($card->cardLastFourDigits !== $last_four_digits) {
        continue;
      }

      return $card;
    }
  }

}
