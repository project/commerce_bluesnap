<?php

namespace Drupal\commerce_bluesnap\PluginForm\Bluesnap;

use Drupal\commerce_bluesnap\Api\ClientFactory;
use Drupal\commerce_bluesnap\BlueSnap\CreditCardHelper as BlueSnapHelper;
use Drupal\commerce_bluesnap\FraudPrevention\FraudSessionInterface;
use Drupal\commerce_bluesnap\Api\HostedPaymentFieldsClientInterface;

use Drupal\commerce\InlineFormManager;
use Drupal\commerce_payment\PluginForm\PaymentMethodAddForm as BasePaymentMethodAddForm;
use Drupal\commerce_store\CurrentStoreInterface;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Routing\RouteMatchInterface;

use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Adds a credit card payment method form for the HostedPaymentFields gateway.
 *
 * @package Drupal\commerce_bluesnap\PluginForm\Bluesnap
 */
class HostedPaymentFieldsPaymentMethodAddForm extends BasePaymentMethodAddForm {

  /**
   * The Bluesnap API client factory.
   *
   * @var \Drupal\commerce_bluesnap\Api\ClientFactory
   */
  protected $clientFactory;

  /**
   * The fraud session service.
   *
   * @var \Drupal\commerce_bluesnap\FraudPrevention\FraudSessionInterface
   */
  protected $fraudSession;

  /**
   * The route match object.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $routeMatch;

  /**
   * Constructs a new HostedPaymentFieldsPaymentMethodAddForm.
   *
   * @param \Drupal\commerce\InlineFormManager $inline_form_manager
   *   The inline form manager.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Psr\Log\LoggerInterface $logger
   *   The logger.
   * @param \Drupal\commerce_bluesnap\Api\ClientFactory $client_factory
   *   The BlueSnap API client factory.
   * @param \Drupal\commerce_bluesnap\FraudPrevention\FraudSessionInterface $fraud_session
   *   The fraud session service.
   * @param \Drupal\commerce_store\CurrentStoreInterface $current_store
   *   The current store.
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The route match object.
   */
  public function __construct(
    InlineFormManager $inline_form_manager,
    EntityTypeManagerInterface $entity_type_manager,
    LoggerInterface $logger,
    ClientFactory $client_factory,
    FraudSessionInterface $fraud_session,
    CurrentStoreInterface $current_store,
    RouteMatchInterface $route_match
  ) {
    parent::__construct(
      $current_store,
      $entity_type_manager,
      $inline_form_manager,
      $logger
    );

    $this->clientFactory = $client_factory;
    $this->fraudSession = $fraud_session;
    $this->routeMatch = $route_match;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('plugin.manager.commerce_inline_form'),
      $container->get('entity_type.manager'),
      $container->get('logger.factory')->get('commerce_payment'),
      $container->get('commerce_bluesnap.client_factory'),
      $container->get('commerce_bluesnap.fraud_session'),
      $container->get('commerce_store.current_store'),
      $container->get('current_route_match')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(
    array $form,
    FormStateInterface $form_state
  ) {
    $form = parent::buildConfigurationForm($form, $form_state);
    $element = &$form['billing_information']['address']['widget'][0];

    // Add the bluesnap attribute to address form elements.
    $element['address_line1']['#attributes']['data-bluesnap'] = 'address_line1';
    $element['address_line2']['#attributes']['data-bluesnap'] = 'address_line2';
    $element['locality']['#attributes']['data-bluesnap'] = 'address_city';
    $element['postal_code']['#attributes']['data-bluesnap'] = 'address_zip';
    $element['country_code']['#attributes']['data-bluesnap'] = 'address_country';

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  protected function buildCreditCardForm(
    array $element,
    FormStateInterface $form_state
  ) {
    // The BlueSnap unique Hosted Payment Fields Token that will be sent in the
    // drupalSettings to the JS.
    // First, initialize BlueSnap.
    if (empty($form_state->getValue('bluesnap_token'))) {
      $plugin = $this->entity->getPaymentGateway()->getPlugin();
      $client = $this->clientFactory->get(
        HostedPaymentFieldsClientInterface::API_ID,
        $plugin->getBluesnapConfig()
      );
      $bluesnap_token = $client->createToken();
    }
    else {
      $bluesnap_token = $form_state->getValue('bluesnap_token');
    }

    // Alter the form with Bluesnap specific needs.
    $element['#attributes']['class'][] = 'bluesnap-form';
    $element['#attached']['library'][] = 'commerce_bluesnap/hosted_payment_fields_form';
    $element['#attached']['drupalSettings']['commerceBluesnap'] = [
      'hostedPaymentFields' => [
        'token' => $bluesnap_token,
      ],
    ];

    // Hidden fields which will be populated by the js.
    $this->hiddenFields($element);
    // The credit card fields necessary for BlueSnap.
    $this->ccFields($element);

    // To display validation errors.
    $element['payment_errors'] = [
      '#type' => 'markup',
      '#markup' => '<div id="payment-errors"></div>',
      '#weight' => -200,
    ];

    // Add bluesnap device datacollector iframe for fraud prevention.
    $element['fraud_prevention'] = $this->deviceDataCollectorIframe();

    return $element;
  }

  /**
   * {@inheritdoc}
   *
   * The JS library performs its own validation, therefore we do not validate
   * the card type, the CVV etc. as it is done in the parent class.
   *
   * We do ensure that the user does not have another card of the same type and
   * with the same last 4 digits. BlueSnap does not provide us with a card ID
   * and the only way to identify cards is by their type/last 4 digits
   * combinations. If the same user has two cards (very unlikely, but it can
   * happen) we could be charging, updating or deleting the wrong card.
   *
   * We therefore do not allow having more than one card with the same type/last
   * 4 digits combination.
   */
  protected function validateCreditCardForm(
    array &$element,
    FormStateInterface $form_state
  ) {
    // We do not need to do the validation if the user is the anonymous
    // user. All cards for anonymous users are stored in separate vaulted
    // shoppers and there should never be conflicts.
    $owner_id = $this->entity->getOwnerId();
    if (!$owner_id) {
      return;
    }

    // Get the card type and number from the form submission.
    $payment_details = $form_state->getValue($element['#parents']);

    $last_four_digits = $payment_details['bluesnap_cc_last_4'];
    $bluesnap_type = $payment_details['bluesnap_cc_type'];
    $map = BlueSnapHelper::creditCardTypeMap();
    if (!isset($map[$bluesnap_type])) {
      $form_state->setError(
        $element,
        $this->t(
          'You have entered a card of an unsupported type. Supported types are:
           Amex, Diners Club, Discover, JCB, Mastercard and VISA.'
        )
      );
      return;
    }
    $card_type = $map[$bluesnap_type];

    // Check if the user has the same card on the same gateway. An application
    // could have multiple gateways for processing credit cards, either
    // connected to different service providers or same service provider
    // e.g. BlueSnap but connected to different BlueSnap accounts - such as for
    // marketplace applications. We therefore only prevent from having the same
    // card on the same Drupal gateway.
    //
    // Theoretically, there could be configurations that have multiple BlueSnap
    // credit card gateways connected to the same BlueSnap account. However,
    // that's an edge case and we don't handle here since it makes the code more
    // complicated. We could address it as a future improvement.
    $storage = $this->entityTypeManager->getStorage('commerce_payment_method');
    $method_ids = $storage->getQuery()
      ->accessCheck(FALSE)
      ->condition('uid', $owner_id)
      ->condition('payment_gateway', $this->entity->getPaymentGatewayId())
      ->condition('card_type', $card_type)
      ->condition('card_number', $last_four_digits)
      ->execute();
    if ($method_ids) {
      $form_state->setError(
        $element,
        $this->t(
          'You have entered a card that is of the same type and has the same
           last 4 digits with another card on your account. Please make sure
           that you are not trying to add a card that already exists on your
           account. Having two cards that match in those details is not
           supported.<br><br>Updating the expiration date or the CVV for an
           existing card is not supported; in such case you may want to delete
           the card and add it again.'
        )
      );
    }
  }

  /**
   * {@inheritdoc}
   */
  protected function submitCreditCardForm(
    array $element,
    FormStateInterface $form_state
  ) {
    // The payment gateway plugin will process the submitted payment details.
  }

  /**
   * Add the BlueSnap hidden fields.
   *
   * @param array $element
   *   The element array.
   */
  protected function hiddenFields(array &$element) {
    // Hidden fields which will be populated by the js once a card is
    // successfully submitted to BlueSnap.
    $element['bluesnap_token'] = [
      '#type' => 'hidden',
      '#attributes' => [
        'id' => 'bluesnap-token',
      ],
    ];

    // These fields will be used for the card details for Anonymous users when
    // creating a payment method.
    $element['bluesnap_cc_type'] = [
      '#type' => 'hidden',
      '#attributes' => [
        'id' => 'bluesnap-cc-type',
      ],
    ];
    $element['bluesnap_cc_last_4'] = [
      '#type' => 'hidden',
      '#attributes' => [
        'id' => 'bluesnap-cc-last-4',
      ],
    ];
    $element['bluesnap_cc_expiry'] = [
      '#type' => 'hidden',
      '#attributes' => [
        'id' => 'bluesnap-cc-expiry',
      ],
    ];
  }

  /**
   * Add the BlueSnap credit card fields.
   *
   * @param array $element
   *   The element array.
   */
  protected function ccFields(array &$element) {
    $element['card_number'] = [
      '#type' => 'item',
      '#title' => t('Card number'),
      '#required' => TRUE,
      '#validated' => TRUE,
      '#markup' => '<div class="form-control hosted-field hosted-field--card-number" id="card-number" data-bluesnap="ccn"></div>',
    ];

    $element['expiration'] = [
      '#type' => 'item',
      '#title' => t('Expiration date'),
      '#required' => TRUE,
      '#validated' => TRUE,
      '#markup' => '<div class="form-control hosted-field hosted-field--exp-date" id="exp-date" data-bluesnap="exp"></div>',
    ];

    $element['security_code'] = [
      '#type' => 'item',
      '#title' => t('CVV'),
      '#required' => TRUE,
      '#validated' => TRUE,
      '#markup' => '<div class="form-control hosted-field hosted-field--cvv" id="cvv" data-bluesnap="cvv"></div>',
    ];
  }

  /**
   * Provides bluesnap device data collector iframe.
   *
   * @return array
   *  Render array which has bluesnap device datacollector iframe markup.
   */
  protected function deviceDataCollectorIframe() {
    $merchant_id = NULL;
    $mode = $this->entity
      ->getPaymentGateway()
      ->getPlugin()
      ->getBluesnapConfig()['env'];

    // Get the Kount merchant ID from the store settings, if we have one
    // available for Enterprise accounts. We use the store for the current
    // order, or the default store if we can't determine the store from the
    // route.
    if ($order = $this->routeMatch->getParameter('commerce_order')) {
      $store = $order->getStore();
    }
    else {
      $store = $this
        ->entityTypeManager
        ->getStorage('commerce_store')
        ->loadDefault();
    }

    $store_config = NULL;
    if ($store->hasField('bluesnap_config')) {
      $store_config = $store->get('bluesnap_config')->value;
    }
    if (!empty($store_config['kount']['merchant_id'])) {
      $merchant_id = $store_config['kount']['merchant_id'];
    }

    return $this->fraudSession->iframe($mode, $merchant_id);
  }

}
